//
//  Array.h
//  CommandLineTool
//
//  Created by Jacobs Laptop  on 09/10/2014.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef H_ARRAY
#define H_ARRAY

class Array
{
public:
    
    Array()
    {
      arraysize = 0;
      fp = nullptr;
    }
    ~Array()
    {
      
    }
    void add (float itemValue)
    {
        // Add new value
        float *fpNew = new float[arraysize + 1];
        
        // Add new data
        for (int i; i < arraysize; i++)
        {
            fpNew[i] = fp[i];
        }
        
        // Add new value
        fpNew[arraysize] = itemValue;
        
        // delete old array
        if (fp != nullptr)
        {
           delete [] fpNew;
        }
        
        // make data point to new array
        fp = fpNew;
        
        arraysize++;
        
        
    }
    float get (int index)
    {
        
        return fp[index];
    }
    int size()
    {
        
        return arraysize;
    }
private:
    
    float *fp = nullptr;
    int arraysize;
    
};


#endif /* H_ARRAY */
